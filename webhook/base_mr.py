"""A basic MR class using Graphql."""
import dataclasses
from functools import cached_property
from textwrap import dedent
from time import sleep
import typing

from cki_lib import misc
from cki_lib.logger import get_logger
import prometheus_client as prometheus

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.description import MRDescription
from webhook.users import UserCache

if typing.TYPE_CHECKING:
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest
    from gitlab.v4.objects.projects import Project

    from webhook.rh_metadata import Branch as RH_Branch
    from webhook.rh_metadata import Project as RH_Project
    from webhook.session import BaseSession
    from webhook.users import User

LOGGER = get_logger('cki.webhook.base_mr')

METRIC_KWF_QUERY_RETRIES = prometheus.Counter(
    'kwf_query_retries',
    'Number of times query data was found on an MR only after retrying'
)


def check_mr_query_results(results: typing.Dict) -> bool:
    """Check the query results are useful."""
    if not results['project']['mr']:
        LOGGER.warning('Merge request does not exist?')
        return False
    return True


@dataclasses.dataclass(repr=False)
class BaseMR:
    # pylint: disable=too-many-public-methods
    """Hold basic MR data and provide basic functions."""

    MR_QUERY_BASE = dedent("""
    query mrDetails(
        $mr_id: String!,
        $namespace: ID!,
        $first: Boolean = true,
        $after: String = ""
        ) {
      project(fullPath: $namespace) {
        id @include(if: $first)
        mr: mergeRequest(iid: $mr_id) {
          approved @include(if: $first)
          author {
            ...GlUser @include(if: $first)
          }
          commitCount @include(if: $first)
          description @include(if: $first)
          global_id: id @include(if: $first)
          ...MrLabelsPaged @include(if: $first)
          state @include(if: $first)
          draft @include(if: $first)
          ...MrFiles @include(if: $first)
          preparedAt @include(if: $first)
          sourceBranch @include(if: $first)
          targetBranch @include(if: $first)
          title @include(if: $first)
          headPipeline {
            id @include(if: $first)
          }
        }
      }
    }
    """)

    MR_QUERY = (MR_QUERY_BASE +
                fragments.GL_USER +
                fragments.MR_LABELS_PAGED +
                fragments.MR_FILES)

    session: 'BaseSession'
    url: str
    raw_data: typing.Dict
    _labels: typing.List[defs.Label] = dataclasses.field(default_factory=list, init=False)

    def __repr__(self) -> str:
        """Describe yourself."""
        is_dependency = getattr(self, 'is_dependency', '???')
        commits = len(getattr(self, 'commits', {})) or self.commit_count
        repr_str = f'MR {self.namespace}!{self.iid}'
        repr_str += f', approved: {self.approved}'
        repr_str += f', commits: {commits}'
        repr_str += f', state: {self.state.name.lower()}'
        repr_str += f', draft: {self.draft}'
        repr_str += f", dependency: {is_dependency}"
        repr_str += f", project: {self.project.name if self.project else 'None'}"
        repr_str += f", branch: {self.branch.name if self.branch else 'None'}"
        return f'<{repr_str}>'

    def __post_init__(self) -> None:
        """Set the labels from the input data."""
        self._labels = make_labels(self.session, self.raw_data, self.namespace, self.iid)
        LOGGER.info('Created %s', self)
        if not misc.get_nested_key(self.raw_data, 'project/mr'):
            LOGGER.warning('No MR data! raw_data is: %s', self.raw_data)

    @classmethod
    def new(cls, session: 'BaseSession', mr_url: str, *args, **kwargs) -> 'BaseMR':
        """Do a query and use its results to construct a new BaseMR."""
        mr_data = cls.do_mr_query(session, mr_url)
        return cls(session, mr_url, mr_data, *args, **kwargs)

    def update(self) -> None:
        """Run the MR_QUERY and replace self.raw_data."""
        # Think about how this may impact cached_properties.
        self.raw_data = self.do_mr_query(self.session, self.url)
        self._labels = make_labels(self.session, self.raw_data, self.namespace, self.iid)

    @classmethod
    def do_mr_query(cls, session: 'BaseSession', mr_url: str) -> typing.Dict:
        """Run the MR_QUERY and return the results dict."""
        mr_url = defs.GitlabURL(mr_url)
        query_params = {'namespace': mr_url.namespace, 'mr_id': str(mr_url.id)}
        return cls.query(session, cls.MR_QUERY, query_params, operation_name='mrDetails')

    @property
    def approved(self) -> bool:
        """Return True if the MR is approved, otherwise False."""
        return misc.get_nested_key(self.raw_data, 'project/mr/approved', False)

    @property
    def author(self) -> typing.Union['User', None]:
        """Return the MR author as a User."""
        author_dict = misc.get_nested_key(self.raw_data, 'project/mr/author')
        return self.user_cache.get(author_dict) if author_dict else None

    @property
    def commit_count(self) -> int:
        """Return the number of commits the MR has if known, or 0."""
        return int(misc.get_nested_key(self.raw_data, 'project/mr/commitCount', 0))

    @cached_property
    def description(self) -> MRDescription:
        """Return the MRDescription object for this MR."""
        description_text = misc.get_nested_key(self.raw_data, 'project/mr/description', '')
        return MRDescription(description_text, self.namespace, self.session.graphql)

    @property
    def draft(self) -> bool:
        """Return True if this MR is a draft, otherwise False."""
        return misc.get_nested_key(self.raw_data, 'project/mr/draft', False)

    @property
    def global_id(self) -> str:
        """Return the Gitlab global ID string of the MR if known, or ''."""
        return misc.get_nested_key(self.raw_data, 'project/mr/global_id', '')

    @property
    def head_pipeline_id(self) -> int:
        """Return the head pipeline ID if known, or 0."""
        head_pipeline = misc.get_nested_key(self.raw_data, 'project/mr/headPipeline')
        return int(head_pipeline['id'].rsplit('/', 1)[-1]) if head_pipeline else 0

    @property
    def labels(self) -> typing.List[defs.Label]:
        """Return the list of MR labels."""
        return self._labels

    @property
    def state(self) -> defs.MrState:
        """Return the MrState."""
        return defs.MrState.from_str(misc.get_nested_key(self.raw_data, 'project/mr/state', ''))

    @property
    def source_branch(self) -> str:
        """Return the source branch name of the MR if known, otherwise ''."""
        return misc.get_nested_key(self.raw_data, 'project/mr/sourceBranch', '')

    @property
    def target_branch(self) -> str:
        """Return the target branch name of the MR if known, otherwise ''."""
        return misc.get_nested_key(self.raw_data, 'project/mr/targetBranch', '')

    @property
    def title(self) -> str:
        """Return the title of the MR if known, otherwise ''."""
        return misc.get_nested_key(self.raw_data, 'project/mr/title', '')

    @property
    def project_id(self) -> int:
        """Return the MR's project ID if known, or 0."""
        project_id = misc.get_nested_key(self.raw_data, 'project/id')
        return int(project_id.split('/')[-1]) if project_id else 0

    @property
    def branch(self) -> typing.Union['RH_Branch', None]:
        """Return the matching Branch object, or None."""
        return self.project.get_branch_by_name(self.target_branch) if self.project else None

    @property
    def files(self) -> typing.List[str]:
        """Return the list of files affected by the MR."""
        return [path['path'] for path in misc.get_nested_key(self.raw_data, 'project/mr/files', [])]

    @cached_property
    def gl_mr(self) -> 'ProjectMergeRequest':
        """Return a gl_mergerequest object."""
        return self.gl_project.mergerequests.get(self.iid)

    @cached_property
    def gl_project(self) -> 'Project':
        """Return a gl_project object."""
        return self.session.get_gl_project(self.namespace)

    @property
    def has_depends(self) -> bool:
        """Return True if the MR description lists any dependencies, otherwise False."""
        return bool(self.description.depends or self.description.depends_mrs)

    @cached_property
    def iid(self) -> int:
        """Return the MR internal ID as an int."""
        return common.parse_mr_url(self.url)[1]

    @cached_property
    def namespace(self) -> str:
        """Return the MR namespace."""
        return common.parse_mr_url(self.url)[0]

    @cached_property
    def project(self) -> typing.Union['RH_Project', None]:
        """Return the matching rh_metadata.Project object, or None."""
        return self.session.rh_projects.get_project_by_namespace(self.namespace)

    @cached_property
    def user_cache(self) -> UserCache:
        """Return a UserCache object."""
        return self.session.get_user_cache(self.namespace)

    def add_labels(self, to_add: typing.List[str], remove_scoped: bool = False) -> None:
        """Use common code to add the list of labels to the MR."""
        fresh_labels = common.add_label_to_merge_request(self.gl_project, self.iid, to_add,
                                                         remove_scoped=remove_scoped)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_add).issubset(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_add: {to_add}'))

    def remove_labels(self, to_remove: typing.List[str]) -> None:
        """Use common code to remove the list of labels from the MR."""
        fresh_labels = common.remove_labels_from_merge_request(self.gl_project, self.iid, to_remove)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_remove).isdisjoint(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_remove: {to_remove}'))

    def labels_with_prefix(self, prefix: str) -> typing.List[defs.Label]:
        """Return the list of scoped MR labels with the given prefix."""
        return [label for label in self.labels if label.scoped and label.prefix == prefix]

    def labels_with_scope(self, scope: defs.MrScope) -> typing.List[defs.Label]:
        """Return the list of scoped MR labels with the given MrScope."""
        return [label for label in self.labels if label.scoped and label.scope is scope]

    @staticmethod
    def query(
        session: 'BaseSession',
        query_string: str,
        query_params: typing.Optional[typing.Dict] = None,
        paged_key: typing.Optional[str] = None,
        check_function: typing.Optional[typing.Callable] = check_mr_query_results,
        query_retries: int = 2,
        query_retry_delay: int = 5,
        operation_name: typing.Optional[str] = None
    ) -> typing.Dict:
        # pylint: disable=too-many-arguments
        """Run the given query and process it with the given function."""
        query_params = query_params or {}
        for retry in range(query_retries):
            results = session.graphql.client.query(
                query_string,
                variable_values=query_params,
                paged_key=paged_key,
                operation_name=operation_name
            )
            if not results:
                LOGGER.warning('No results for query with params: %s\n%s', query_params,
                               query_string)
                return None
            if check_function(results):
                if retry:
                    LOGGER.warning('API returned data on the second try 🙄.')
                    METRIC_KWF_QUERY_RETRIES.inc()
                break
            if retry:
                LOGGER.info('Still no data after retry 🤷.')
                break
            LOGGER.info('No data found, trying again in %s seconds.', query_retry_delay)
            sleep(query_retry_delay)
        return results


def make_labels(
    session: 'BaseSession',
    raw_data: typing.Dict,
    namespace: str,
    mr_id: int
) -> typing.List[defs.Label]:
    """Return the list of Label objects in the raw_data."""
    raw_labels = misc.get_nested_key(raw_data, 'project/mr/labels', {})
    return session.graphql.get_all_mr_labels(namespace, mr_id, raw_labels) if \
        raw_labels else []
